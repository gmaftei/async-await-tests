const gulp = require('gulp');
const browserSync = require('browser-sync');
const reload = browserSync.reload;
const nodemon = require('gulp-nodemon');
const babel = require('gulp-babel');
const apidoc = require('gulp-apidoc');


gulp.task('default', ['browser-sync'], function () {
    gulp.watch(["./src/**/*.*"], reload);
});


gulp.task('browser-sync', ['nodemon'], function() {
    browserSync.init(null, {
        proxy: "http://localhost:3300", 
        
    });
});



gulp.task('nodemon', ['babel'], function (cb) {
//gulp.task('nodemon', [], function (cb) {
    var callbackCalled = false;
    return nodemon({script: './index.js'}).on('start', function () {
        if (!callbackCalled) {
            callbackCalled = true;
            cb();
        }
    });
});

gulp.task('babel', () =>
    gulp.src('src/app.js')
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulp.dest('dist'))
);

gulp.task('apidoc',function(done){
              apidoc({
                  src: ["src/routes", "src/middleware"],
                  dest: "public/apidoc/html",
                  template: "public/apidoc/template-black",
                  debug: true,
                  includeFilters: [ ".*\\.js$" ],
                  config: ''
              },done);
});