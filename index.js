/** Express router providing user related routes
 * @module routers/users
 * @requires express
 */


const express = require('express')
const app = express()
const morgan = require('morgan')
const path = require('path')
const glob = require('glob')
const requireAll = require('require-all')
const fs = require('fs')
const bodyParser = require('body-parser')




// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream( path.join(__dirname, 'access.log'), {flags: 'a'} )


app.use(bodyParser.json());
// express.json()

app.use(morgan('combined', {stream: accessLogStream}))



//require all routes in one step ( from /src/routes directory )
glob.sync( __dirname + '/src/routes/*.js' ).map(  file  => {  require( path.resolve( file ) )(app)  })

// Doc(app)


 app.listen(3300, function () {


console.log('Express server listening on port 3300!')
})
 