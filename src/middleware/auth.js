const sequelize = require(__dirname + './../services/sequelize-service')

module.exports = function(req, res, next) {
/**
 * @apiVersion 1.0.0
 * @api {middleware}  __ auth
 * @apiPermission MIDDLEWARE
 * @apiName secure private endpoints
 * @apiGroup MIDDLEWARE
 * @apiExample {js} Example usage:
            app.post('/route', auth, (req,res) => {...})


 * @apiSuccess (Success) {Object}  none  adds usedata to request (usr.req) then next()
 * @apiError  (Eroare) {Object}  none  sends status 401; res.end()


 */	

    let isLoggedIn = sequelize.api.find({
        where: {
        	 apikey: req.headers.authorization

            
        }
    })

    isLoggedIn.then( response => {
    	
	    if (response == null) {
	        res.sendStatus(401)
	        res.end()
	    } else {

	        let userdata = JSON.parse(response.userdata)

	        req.usr = userdata.user
	       
	        next()
	    }    	
    })


}

