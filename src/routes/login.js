const path = require('path');

const  	login = require( '../services/login-service')
const 	sequelize = require(__dirname + './../services/sequelize-service')
const 	moment = require('moment')

module.exports = function(app) {
	
/**
 * @apiVersion 1.0.0
 * @api {post} /login User Login
 * @apiDescription the response status is 200 for succesful authentication, the response body containing all user's relevant data and a 401 response is received upon rejection and aditional information is sent in response's body ({ok: false, msg: reason for rejection}) 

 * @apiPermission PUBLIC
 * @apiName login
 * @apiGroup AUTHENTICATION

 * @apiParam {Object} requestObject obiect care contine numele de utilizator si parola de acces in aplicatie	
 * @apiParam {String} username numele utilizatorului
 * @apiParam {String} password parola setata 
 *
 *@apiParamExample EXEMPLU OBIECT DE AUTENTIFICARE
 {"username": "demo", "password": "1234"}

 * @apiSuccess (SUCCES - status 200) {Object} -
 * @apiSuccess (SUCCES - status 200) {Boolean} ok  true
 * @apiSuccess (SUCCES - status 200) {Json} data contains all relevant info about the logged-in user


 * @apiError (Credențiale lipsă sau eronate - întoarce status 401!)  {String} 401-rejection
 */

	// async await approach
	app.post('/login',  async (req, res) => {
		
		try {
		 	const response = await login(req.body)
		 	const result = {}

		 	if ( response.statusCode == 200 && response.body.ok == true) {
		 		let apiRow = {
		 			uid: response.body.result.uid,
		 			userdata: JSON.stringify(response.body.result),
		 			created: moment().unix(),
		 			apikey: response.body.result.hash
		 		}
		 		await sequelize.api.create(apiRow)
		 		res.statusCode = 200
		 		res.send(response.body.result)			
		 	} else {
		 		res.statusCode = 401
		 		res.send({ok: false, msg: "wrong username or password"})
		 	}
		 } catch(err) {return err}

		 	
		})



}