const request = require('request-promise')


// async - await based 
module.exports =  async data => {
		try {

			let rq =  await request.post({
					url: 'https://auth.de3.undevreau.eu/login', 
					timeout: 120000,
					json: true,
					body:data,
					resolveWithFullResponse: true
				})
			return rq
		} catch(err) {
			return err
		}
}
